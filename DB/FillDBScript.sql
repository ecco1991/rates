USE [Test]
GO

INSERT INTO [dbo].[Currencies]
           ([Id]
           ,[Name])
     VALUES
           ('R01010'
           ,'������������� ������')
INSERT INTO [dbo].[Currencies]
           ([Id]
           ,[Name])
     VALUES
           ('R01090B'
           ,'����������� �����')
INSERT INTO [dbo].[Currencies]
           ([Id]
           ,[Name])
     VALUES
           ('R01100'
           ,'���������� ���')
GO

USE [Test]
GO
--������������� ������
INSERT INTO [dbo].[Rates]
           ([CurrencyId]
           ,[Date]
           ,[Rate])
     VALUES
           ('R01010'
           ,CONVERT(datetime2,'01.04.2021')
           ,0.1)
INSERT INTO [dbo].[Rates]
           ([CurrencyId]
           ,[Date]
           ,[Rate])
     VALUES
           ('R01010'
           ,CONVERT(datetime2,'02.04.2021')
           ,2)
INSERT INTO [dbo].[Rates]
           ([CurrencyId]
           ,[Date]
           ,[Rate])
     VALUES
           ('R01010'
           ,CONVERT(datetime2,'03.04.2021')
           ,300)


--����������� �����
INSERT INTO [dbo].[Rates]
           ([CurrencyId]
           ,[Date]
           ,[Rate])
     VALUES
           ('R01090B'
           ,CONVERT(datetime2,'04.04.2021')
           ,32.4178)
INSERT INTO [dbo].[Rates]
           ([CurrencyId]
           ,[Date]
           ,[Rate])
     VALUES
           ('R01090B'
           ,CONVERT(datetime2,'05.04.2021')
           ,35.4178)
INSERT INTO [dbo].[Rates]
           ([CurrencyId]
           ,[Date]
           ,[Rate])
     VALUES
           ('R01090B'
           ,CONVERT(datetime2,'06.04.2021')
           ,38.4178)


--���������� ���
INSERT INTO [dbo].[Rates]
           ([CurrencyId]
           ,[Date]
           ,[Rate])
     VALUES
           ('R01100'
           ,CONVERT(datetime2,'07.04.2021')
           ,31.4178)
INSERT INTO [dbo].[Rates]
           ([CurrencyId]
           ,[Date]
           ,[Rate])
     VALUES
           ('R01100'
           ,CONVERT(datetime2,'08.04.2021')
           ,36.4178)
INSERT INTO [dbo].[Rates]
           ([CurrencyId]
           ,[Date]
           ,[Rate])
     VALUES
           ('R01100'
           ,CONVERT(datetime2,'09.04.2021')
           ,42.8101)

GO


