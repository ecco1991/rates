CREATE FUNCTION dbo.GetRate (@currencyName CHAR(50), @date CHAR(10))
	RETURNS FLOAT
BEGIN
	DECLARE @convertedDate DATETIME2(7) = CONVERT(datetime2, @date)
	DECLARE @Rate FLOAT
	SET @Rate = (
	SELECT TOP(1) Rate FROM [Test].[dbo].[Rates] as rates
	INNER JOIN [Test].[dbo].[Currencies] as currencies on currencies.Id = rates.CurrencyId
	WHERE rates.Date = @convertedDate
	)
	RETURN @Rate
END

--DROP FUNCTION dbo.GetRate