﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;


namespace test.Models
{
    public class Valute
    {
        [XmlAttribute("ID")]
        public string Id { get; set; }
        public string NumCode { get; set; }
        public string CharCode { get; set; }
        public int Nominal { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }

        [XmlIgnore]
        public DateTime targetDate { get; set; }
    }
}
