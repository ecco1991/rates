﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;


namespace test.Models
{
    public class Item
    {
        [XmlAttribute("ID")]
        public string Id { get; set; }
        public string Name { get; set; }
        public string EngName { get; set; }
        public string Nominal { get; set; }
        public string ParentCode { get; set; }
        public string ISO_Num_Code { get; set; }
        public string ISO_Char_Code { get; set; }
    }
}
