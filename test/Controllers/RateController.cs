﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml.Serialization;
using test.Models;

namespace test.Controllers
{
    public class RateController : Controller
    {
        public RateController(IMemoryCache memmoryCache)
        {
            cache = memmoryCache;
        }

        private const string currenciesDictionaryUrl = @"http://www.cbr.ru/scripts/XML_valFull.asp";
        private const string targetUrl = "http://www.cbr.ru/scripts/XML_daily.asp?date_req={0}";

        // ключ словаря валют в кэше
        private readonly Guid currenciesDictionaryKey = new Guid("{0d1090c7-785d-449d-90bc-0bb9d700f1f6}");
        private IMemoryCache cache;

        public IActionResult Index()
        {
            return View();
        }
        
        [HttpGet]
        [ActionName("CalculateRate")]
        public IActionResult GetForm()
        {
            Dictionary<string, Item> currenciesDictionary;
            if (!cache.TryGetValue(currenciesDictionaryKey, out currenciesDictionary))
            {
                var responseString = GetResponseFromCBR(currenciesDictionaryUrl);
                var currenciesCollection = DeserializeXML<List<Item>>(responseString, "Valuta");
                currenciesDictionary = new Dictionary<string, Item>();
                currenciesCollection?.ForEach(x =>
                {
                    currenciesDictionary.Add(x.Id, x);
                });
                //в кэш на 5 минут
                cache.Set(currenciesDictionaryKey, currenciesDictionary,
                    new MemoryCacheEntryOptions
                    {
                        AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(5)
                    });
            }

            return View("SimpleInputForm", currenciesDictionary);
        }

        [HttpPost]
        [ActionName("CalculateRate")]
        public IActionResult GetCalculatedRateByDate(string currencyId, DateTime targetDate)
        {
            var url = String.Format(targetUrl, targetDate);
            var responseString = GetResponseFromCBR(url);
            var rate = DeserializeXML<List<Valute>>(responseString, "ValCurs")
                .FirstOrDefault(x => x.Id == currencyId);

            if (rate == null)
                return Error("Ответ от ЦБР не содержит данных курса рубля относительно выбранной валюты.");           

            rate.targetDate = targetDate;
            return View("Rate", rate);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error(string message)
        {
            return View("Error", message);
        }

        /// <summary>
        /// Потенциально выносится в сервис
        /// </summary>
        /// <param name="url"></param>
        /// <param name="targetDate"></param>
        /// <returns></returns>
        private string GetResponseFromCBR(string url, DateTime? targetDate = null)
        {
            if (targetDate != null)
            {
                url = String.Format(url, targetDate);
            }

            string xmlString;
            using (var client = new HttpClient())
            {
                var response = client.GetAsync(url).Result;

                byte[] bytes = response.Content.ReadAsByteArrayAsync().Result;
                Encoding encoding = CodePagesEncodingProvider.Instance.GetEncoding(1251);
                xmlString = encoding.GetString(bytes, 0, bytes.Length);
            }
            return xmlString;
        }

        /// <summary>
        /// Потенциально выносится в сервис
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xml"></param>
        /// <returns></returns>
        private T DeserializeXML<T>(string xml, string rootElementName)
        {
            var rootElement = new XmlRootAttribute(rootElementName);
            XmlSerializer serializer = new XmlSerializer(typeof(T), rootElement);
            StringReader stringReader = new StringReader(xml);
            var result = (T)serializer.Deserialize(stringReader);
            return result;
        }
    }
}
